<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PatientsController extends AbstractController
{
    /**
     * @Route("/patients", name="patients")
     */
    public function index()
    {
        return $this->render('patients/index.html.twig', [
            'controller_name' => 'PatientsController',
        ]);
    }
}
