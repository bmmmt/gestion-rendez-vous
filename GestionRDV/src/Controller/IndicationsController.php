<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndicationsController extends AbstractController
{
    /**
     * @Route("/indications", name="indications")
     */
    public function index()
    {
        return $this->render('indications/index.html.twig', [
            'controller_name' => 'IndicationsController',
        ]);
    }
}
