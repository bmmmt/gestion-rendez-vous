<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndisponibilitesController extends AbstractController
{
    /**
     * @Route("/indisponibilites", name="indisponibilites")
     */
    public function index()
    {
        return $this->render('indisponibilites/index.html.twig', [
            'controller_name' => 'IndisponibilitesController',
        ]);
    }
}
