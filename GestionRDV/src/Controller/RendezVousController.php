<?php

namespace App\Controller;

use App\Entity\Patients;
use App\Entity\Personnels;
use App\Entity\RendezVous;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

class RendezVousController extends AbstractController
{
    /**
     * @Route("/rendez/vous", name="rendez_vous")
     */
    public function index()
    {
        return $this->render('rendez_vous/index.html.twig', [
            'controller_name' => 'RendezVousController',
        ]);
    }


    /**
     * @Route("/validation-rendez-vous", name="validation_rendez_vous")
     */
    public function afficherRendezVousDemandes()
    {
        $repository = $this->getDoctrine()->getRepository(RendezVous::class);
        $lesRendezVous = $repository->findAll();

        //Formulaire permettant de valider ou supprimer un rendez vous
        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
            ->add('Valider', ButtonType::class)
            ->getForm();

        return $this->render('rendez_vous/validation-rendez-vous.html.twig', [
            'lesRendezVous' => $lesRendezVous,

        ]);
    }

    /**
     * @Route("/creer-rendez-vous", name="creer_rendez_vous")
     */
    public function creerRendezVous(Request $request)
    {
        $requete = "select nom from user where id =(select id_personnes from personnels)";
        

        $unRendezVous = new RendezVous();
        $form = $this->createFormBuilder($unRendezVous)
            ->add('date', DateType::class, array('label' => 'Date : '))
            ->add('heureDebut', TimeType::class, array('label' => 'Heure début rendez vous : '))
            ->add('heureFin', TimeType::class, array('label' => 'Heure fin rendez vous : '))
            ->add('le_patient', EntityType::class, array(
                'class' => user::class,
                'choice_label' => 'nom'
            ))
            ->add('le_medecin', EntityType::class, array(
                'class' => user::class,
                'choice_label' => 'nom'


            ))
            ->add('save', SubmitType::class, array('label' => 'Ajouter Rendez Vous'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $Patients = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($unRendezVous);
            $em->flush();
            return $this->redirectToRoute('creer_rendez_vous');
        }
        return $this->render('rendez_vous/creer-rendez-vous.html.twig', array(
            'form' => $form->createView(),
        ));
    }

 /**
     * @Route("/priseRDV", name="priseRDV")
     */
    public function priseRDV()
    {
        return $this->render('rendez_vous/priseRDV.twig', [
            'controller_name' => 'RendezVousController',
        ]);
    }

}
