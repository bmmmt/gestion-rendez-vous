<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TraitementsController extends AbstractController
{
    /**
     * @Route("/traitements", name="traitements")
     */
    public function index()
    {
        return $this->render('traitements/index.html.twig', [
            'controller_name' => 'TraitementsController',
        ]);
    }
}
