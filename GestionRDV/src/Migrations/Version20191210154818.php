<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191210154818 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE indisponibilites (id INT AUTO_INCREMENT NOT NULL, le_personnel_id INT DEFAULT NULL, date_debut DATETIME DEFAULT NULL, date_fin DATETIME DEFAULT NULL, libelle VARCHAR(250) DEFAULT NULL, INDEX IDX_C684A4C19159BAFA (le_personnel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personnels (id INT AUTO_INCREMENT NOT NULL, le_role_id INT DEFAULT NULL, nom VARCHAR(150) DEFAULT NULL, prenom VARCHAR(150) DEFAULT NULL, INDEX IDX_7AC38C2BF9CC54DB (le_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rendez_vous (id INT AUTO_INCREMENT NOT NULL, le_medecin_id INT DEFAULT NULL, le_patient_id INT DEFAULT NULL, date DATE DEFAULT NULL, heure_debut TIME DEFAULT NULL, heure_fin TIME DEFAULT NULL, INDEX IDX_65E8AA0A27F3652F (le_medecin_id), INDEX IDX_65E8AA0A4889EDD2 (le_patient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE indisponibilites ADD CONSTRAINT FK_C684A4C19159BAFA FOREIGN KEY (le_personnel_id) REFERENCES personnels (id)');
        $this->addSql('ALTER TABLE personnels ADD CONSTRAINT FK_7AC38C2BF9CC54DB FOREIGN KEY (le_role_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE rendez_vous ADD CONSTRAINT FK_65E8AA0A27F3652F FOREIGN KEY (le_medecin_id) REFERENCES personnels (id)');
        $this->addSql('ALTER TABLE rendez_vous ADD CONSTRAINT FK_65E8AA0A4889EDD2 FOREIGN KEY (le_patient_id) REFERENCES patients (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE indisponibilites DROP FOREIGN KEY FK_C684A4C19159BAFA');
        $this->addSql('ALTER TABLE rendez_vous DROP FOREIGN KEY FK_65E8AA0A27F3652F');
        $this->addSql('ALTER TABLE personnels DROP FOREIGN KEY FK_7AC38C2BF9CC54DB');
        $this->addSql('DROP TABLE indisponibilites');
        $this->addSql('DROP TABLE personnels');
        $this->addSql('DROP TABLE rendez_vous');
        $this->addSql('DROP TABLE roles');
    }
}
