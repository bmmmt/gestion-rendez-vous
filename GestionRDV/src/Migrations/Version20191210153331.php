<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191210153331 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE indications (id INT AUTO_INCREMENT NOT NULL, le_medicament_id INT DEFAULT NULL, le_traitement_id INT DEFAULT NULL, posologie VARCHAR(150) NOT NULL, INDEX IDX_368D819D687B66C (le_medicament_id), INDEX IDX_368D819A029932D (le_traitement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE medicaments (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patients (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(150) DEFAULT NULL, prenom VARCHAR(150) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traitements (id INT AUTO_INCREMENT NOT NULL, le_patient_id INT DEFAULT NULL, date_debut DATE DEFAULT NULL, duree INT DEFAULT NULL, INDEX IDX_BE2E4F0D4889EDD2 (le_patient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE indications ADD CONSTRAINT FK_368D819D687B66C FOREIGN KEY (le_medicament_id) REFERENCES medicaments (id)');
        $this->addSql('ALTER TABLE indications ADD CONSTRAINT FK_368D819A029932D FOREIGN KEY (le_traitement_id) REFERENCES traitements (id)');
        $this->addSql('ALTER TABLE traitements ADD CONSTRAINT FK_BE2E4F0D4889EDD2 FOREIGN KEY (le_patient_id) REFERENCES patients (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE indications DROP FOREIGN KEY FK_368D819D687B66C');
        $this->addSql('ALTER TABLE traitements DROP FOREIGN KEY FK_BE2E4F0D4889EDD2');
        $this->addSql('ALTER TABLE indications DROP FOREIGN KEY FK_368D819A029932D');
        $this->addSql('DROP TABLE indications');
        $this->addSql('DROP TABLE medicaments');
        $this->addSql('DROP TABLE patients');
        $this->addSql('DROP TABLE traitements');
    }
}
