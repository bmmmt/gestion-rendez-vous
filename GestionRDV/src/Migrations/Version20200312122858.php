<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200312122858 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE etats (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(64) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE indications (id INT AUTO_INCREMENT NOT NULL, le_medicament_id INT DEFAULT NULL, le_traitement_id INT DEFAULT NULL, posologie VARCHAR(150) NOT NULL, INDEX IDX_368D819D687B66C (le_medicament_id), INDEX IDX_368D819A029932D (le_traitement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE indisponibilites (id INT AUTO_INCREMENT NOT NULL, le_personnel_id INT DEFAULT NULL, date_debut DATETIME DEFAULT NULL, date_fin DATETIME DEFAULT NULL, libelle VARCHAR(250) DEFAULT NULL, INDEX IDX_C684A4C19159BAFA (le_personnel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE medicaments (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patients (id INT AUTO_INCREMENT NOT NULL, id_personnes INT NOT NULL,  INDEX IDX_2CCC2E2C37ABA2CE (id_personnes), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personnels (id INT AUTO_INCREMENT NOT NULL, le_role_id INT DEFAULT NULL, id_personnes INT NOT NULL, INDEX IDX_7AC38C2BF9CC54DB (le_role_id), INDEX IDX_7AC38C2B37ABA2CE (id_personnes), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rendez_vous (id INT AUTO_INCREMENT NOT NULL, le_medecin_id INT DEFAULT NULL, le_patient_id INT DEFAULT NULL, id_etats_id INT DEFAULT NULL, date DATE DEFAULT NULL, heure_debut TIME DEFAULT NULL, heure_fin TIME DEFAULT NULL, motif VARCHAR(512) DEFAULT NULL, INDEX IDX_65E8AA0A27F3652F (le_medecin_id), INDEX IDX_65E8AA0A4889EDD2 (le_patient_id), INDEX IDX_65E8AA0A9AA5E0BB (id_etats_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traitements (id INT AUTO_INCREMENT NOT NULL, le_patient_id INT DEFAULT NULL, date_debut DATE DEFAULT NULL, duree INT DEFAULT NULL, INDEX IDX_BE2E4F0D4889EDD2 (le_patient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE indications ADD CONSTRAINT FK_368D819D687B66C FOREIGN KEY (le_medicament_id) REFERENCES medicaments (id)');
        $this->addSql('ALTER TABLE indications ADD CONSTRAINT FK_368D819A029932D FOREIGN KEY (le_traitement_id) REFERENCES traitements (id)');
        $this->addSql('ALTER TABLE indisponibilites ADD CONSTRAINT FK_C684A4C19159BAFA FOREIGN KEY (le_personnel_id) REFERENCES personnels (id)');
        $this->addSql('ALTER TABLE patients ADD CONSTRAINT FK_2CCC2E2C37ABA2CE FOREIGN KEY (id_personnes) REFERENCES user (id)');
        $this->addSql('ALTER TABLE personnels ADD CONSTRAINT FK_7AC38C2BF9CC54DB FOREIGN KEY (le_role_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE personnels ADD CONSTRAINT FK_7AC38C2B37ABA2CE FOREIGN KEY (id_personnes) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rendez_vous ADD CONSTRAINT FK_65E8AA0A27F3652F FOREIGN KEY (le_medecin_id) REFERENCES personnels (id)');
        $this->addSql('ALTER TABLE rendez_vous ADD CONSTRAINT FK_65E8AA0A4889EDD2 FOREIGN KEY (le_patient_id) REFERENCES patients (id)');
        $this->addSql('ALTER TABLE rendez_vous ADD CONSTRAINT FK_65E8AA0A9AA5E0BB FOREIGN KEY (id_etats_id) REFERENCES etats (id)');
        $this->addSql('ALTER TABLE traitements ADD CONSTRAINT FK_BE2E4F0D4889EDD2 FOREIGN KEY (le_patient_id) REFERENCES patients (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rendez_vous DROP FOREIGN KEY FK_65E8AA0A9AA5E0BB');
        $this->addSql('ALTER TABLE indications DROP FOREIGN KEY FK_368D819D687B66C');
        $this->addSql('ALTER TABLE patients DROP FOREIGN KEY FK_2CCC2E2C37ABA2CE');
        $this->addSql('ALTER TABLE personnels DROP FOREIGN KEY FK_7AC38C2B37ABA2CE');
        $this->addSql('ALTER TABLE rendez_vous DROP FOREIGN KEY FK_65E8AA0A4889EDD2');
        $this->addSql('ALTER TABLE traitements DROP FOREIGN KEY FK_BE2E4F0D4889EDD2');
        $this->addSql('ALTER TABLE indisponibilites DROP FOREIGN KEY FK_C684A4C19159BAFA');
        $this->addSql('ALTER TABLE rendez_vous DROP FOREIGN KEY FK_65E8AA0A27F3652F');
        $this->addSql('ALTER TABLE personnels DROP FOREIGN KEY FK_7AC38C2BF9CC54DB');
        $this->addSql('ALTER TABLE indications DROP FOREIGN KEY FK_368D819A029932D');
        $this->addSql('DROP TABLE etats');
        $this->addSql('DROP TABLE indications');
        $this->addSql('DROP TABLE indisponibilites');
        $this->addSql('DROP TABLE medicaments');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE patients');
        $this->addSql('DROP TABLE personnels');
        $this->addSql('DROP TABLE rendez_vous');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE traitements');
    }
}
