<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200311074655 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE patients ADD nom VARCHAR(255) DEFAULT NULL, ADD prenom VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD passwords VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE personnels ADD nom VARCHAR(255) DEFAULT NULL, ADD prenom VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD passwords VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE rendez_vous ADD id_etats INT DEFAULT NULL');
        $this->addSql('ALTER TABLE rendez_vous ADD CONSTRAINT FK_65E8AA0A9AA5E0BB FOREIGN KEY (id_etats_id) REFERENCES etats (id)');
        $this->addSql('CREATE INDEX IDX_65E8AA0A9AA5E0BB ON rendez_vous (id_etats_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE patients DROP nom, DROP prenom, DROP email, DROP passwords');
        $this->addSql('ALTER TABLE personnels DROP nom, DROP prenom, DROP email, DROP passwords');
        $this->addSql('ALTER TABLE rendez_vous DROP FOREIGN KEY FK_65E8AA0A9AA5E0BB');
        $this->addSql('DROP INDEX IDX_65E8AA0A9AA5E0BB ON rendez_vous');
        $this->addSql('ALTER TABLE rendez_vous DROP id_etats_id');
    }
}
