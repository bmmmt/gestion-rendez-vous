<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IndicationsRepository")
 */
class Indications
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $posologie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Medicaments")
     */
    private $le_medicament;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Traitements")
     */
    private $le_traitement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosologie(): ?string
    {
        return $this->posologie;
    }

    public function setPosologie(string $posologie): self
    {
        $this->posologie = $posologie;

        return $this;
    }

    public function getLeMedicament(): ?Medicaments
    {
        return $this->le_medicament;
    }

    public function setLeMedicament(?Medicaments $le_medicament): self
    {
        $this->le_medicament = $le_medicament;

        return $this;
    }

    public function getLeTraitement(): ?Traitements
    {
        return $this->le_traitement;
    }

    public function setLeTraitement(?Traitements $le_traitement): self
    {
        $this->le_traitement = $le_traitement;

        return $this;
    }
}
