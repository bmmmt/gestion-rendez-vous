<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RendezVousRepository")
 */
class RendezVous
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heureDebut;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heureFin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personnels")
     */
    private $le_medecin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patients")
     */
    private $le_patient;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $motif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\etats")
     */
    private $id_etats;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getHeureDebut(): ?\DateTimeInterface
    {
        return $this->heureDebut;
    }

    public function setHeureDebut(?\DateTimeInterface $heureDebut): self
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getHeureFin(): ?\DateTimeInterface
    {
        return $this->heureFin;
    }

    public function setHeureFin(?\DateTimeInterface $heureFin): self
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    public function getLeMedecin(): ?Personnels
    {
        return $this->le_medecin;
    }

    public function setLeMedecin(?Personnels $le_medecin): self
    {
        $this->le_medecin = $le_medecin;

        return $this;
    }

    public function getLePatient(): ?Patients
    {
        return $this->le_patient;
    }

    public function setLePatient(?Patients $le_patient): self
    {
        $this->le_patient = $le_patient;

        return $this;
    }

    public function getMotif(): ?string
    {
        return $this->motif;
    }

    public function setMotif(?string $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getIdEtats(): ?etats
    {
        return $this->id_etats;
    }

    public function setIdEtats(?etats $id_etats): self
    {
        $this->id_etats = $id_etats;

        return $this;
    }
}
