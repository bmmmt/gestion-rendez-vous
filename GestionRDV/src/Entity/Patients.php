<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientsRepository")
 */
class Patients extends Personnes
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personnes", inversedBy="patients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_personnes;

    public function getIdPersonnes(): ?Personnes
    {
        return $this->id_personnes;
    }

    public function setIdPersonnes(?Personnes $id_personnes): self
    {
        $this->id_personnes = $id_personnes;

        return $this;
    }
}
