<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonnelsRepository")
 */
class Personnels extends Personnes
{

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Roles")
     */
    private $le_role;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personnes", inversedBy="personnels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_personnes;

    public function getLeRole(): ?Roles
    {
        return $this->le_role;
    }

    public function setLeRole(?Roles $le_role): self
    {
        $this->le_role = $le_role;

        return $this;
    }

    public function getIdPersonnes(): ?Personnes
    {
        return $this->id_personnes;
    }

    public function setIdPersonnes(?Personnes $id_personnes): self
    {
        $this->id_personnes = $id_personnes;

        return $this;
    }
}
